### hoardorr

hoard**o**rr will hoard all the binaries we need for dockerfiles or packer images.

Some of them it builds, some of them it copies, but absolutely all of them it hoards.

You never know when you run out of them.


### List of artifacts

- `goss` for two architectures: amd64 and arm64
