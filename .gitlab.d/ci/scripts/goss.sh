#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# goss.sh: compile goss for our specific architecures and store those as artifacts
# Usage: bash path/to/goss.sh

git clone 'https://github.com/aelsabbahy/goss.git'
cd goss || exit
git checkout "${GOSS_VERSION}"
git checkout master -- Makefile

# shellcheck disable=2183,2086
sed -i "s/^build: .*/build: $(printf "release\/goss-%s release\/goss-%s" ${MUCHOS_ARCHES//\//-})/" Makefile

export TRAVIS_TAG="${GOSS_VERSION}"

make build
